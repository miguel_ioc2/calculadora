import java.util.Scanner;

public class Calculadora {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Introdueix el primer número: ");
        double num1 = scanner.nextDouble();

        System.out.println("Introdueix el segon número: ");
        double num2 = scanner.nextDouble();

        //Suma
        System.out.println("Suma: " + (num1+num2));

        //Resta
        System.out.println("Resta: " + (num1-num2));

        //Multiplicació
        System.out.println("Multiplicació" + (num1*num2));

        //Divisió
        System.out.println("Divisió" + dividir(num1, num2));

        scanner.close();
    }

    public static double dividir(double a, double b) throws ArithmeticException {
        if (b == 0) {
            throw new ArithmeticException("El divisor no pot ser zero");
        }
        return a / b;
    }
}
